class MovieModel {
  int page;
  List<Results> results;
  int totalPages;
  int totalResults;

  MovieModel({this.page, this.results, this.totalPages, this.totalResults});

  MovieModel.fromJson(Map<String, dynamic> json) {
    page = json['page'];
    if (json['results'] != null) {
      results = <Results>[];
      json['results'].forEach((v) {
        results.add(new Results.fromJson(v));
      });
    }
    totalPages = json['total_pages'];
    totalResults = json['total_results'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['page'] = this.page;
    if (this.results != null) {
      data['results'] = this.results.map((v) => v.toJson()).toList();
    }
    data['total_pages'] = this.totalPages;
    data['total_results'] = this.totalResults;
    return data;
  }
}

class Results {
  double voteAverage;
  String overview;
  DateTime releaseDate;
  bool adult;
  String backdropPath;
  int voteCount;
  List<int> genreIds;
  int id;
  String originalLanguage;
  String originalTitle;
  String posterPath;
  String title;
  bool video;
  double popularity;
  String mediaType;
  String originalName;
  String name;
  List<String> originCountry;
  String firstAirDate;

  Results(
      {this.voteAverage,
      this.overview,
      this.releaseDate,
      this.adult,
      this.backdropPath,
      this.voteCount,
      this.genreIds,
      this.id,
      this.originalLanguage,
      this.originalTitle,
      this.posterPath,
      this.title,
      this.name,
      this.video,
      this.popularity,
      this.mediaType,
      this.originalName,
      this.originCountry,
      this.firstAirDate});

  factory Results.fromJson(Map<String, dynamic> json) => Results(
        adult: json["adult"] == null ? null : json["adult"],
        backdropPath: json["backdrop_path"],
        genreIds: List<int>.from(json["genre_ids"].map((x) => x)),
        originalLanguage: json["original_language"],
        originalTitle:
            json["original_title"] == null ? null : json["original_title"],
        posterPath: json["poster_path"],
        video: json["video"] == null ? null : json["video"],
        voteAverage: json["vote_average"].toDouble(),
        voteCount: json["vote_count"],
        overview: json["overview"],
        id: json["id"],
        releaseDate: json["release_date"] == null
            ? null
            : DateTime.parse(json["release_date"]),
        title: json["title"] == null ? null : json["title"],
        popularity: json["popularity"].toDouble(),
        originCountry: json["origin_country"] == null
            ? null
            : List<String>.from(json["origin_country"].map((x) => x)),
        name: json["name"] == null ? null : json["name"],
        originalName:
            json["original_name"] == null ? null : json["original_name"],
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['vote_average'] = this.voteAverage;
    data['overview'] = this.overview;
    data['release_date'] = this.releaseDate;
    data['adult'] = this.adult;
    data['backdrop_path'] = this.backdropPath;
    data['vote_count'] = this.voteCount;
    data['genre_ids'] = this.genreIds;
    data['id'] = this.id;
    data['original_language'] = this.originalLanguage;
    data['original_title'] = this.originalTitle;
    data['poster_path'] = this.posterPath;
    data['title'] = this.title;
    data['video'] = this.video;
    data['popularity'] = this.popularity;
    data['media_type'] = this.mediaType;
    data['original_name'] = this.originalName;
    data['origin_country'] = this.originCountry;
    data['first_air_date'] = this.firstAirDate;
    return data;
  }
}
