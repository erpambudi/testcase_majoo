import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:majootestcase/db/database_helper.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  DatabaseHelper db = DatabaseHelper();
  bool _isUserLogged = false;
  bool get isUserLogged => _isUserLogged;

  void fetchHistoryLogin() async {
    emit(AuthBlocLoadingState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool isLoggedIn = sharedPreferences.getBool(Preference.STATUS_LOGIN);
    if (isLoggedIn == null) {
      _isUserLogged = false;
      emit(AuthBlocLoginState());
    } else {
      if (isLoggedIn) {
        _isUserLogged = true;
        emit(AuthBlocLoggedInState());
      } else {
        _isUserLogged = false;
        emit(AuthBlocLoginState());
      }
    }
  }

  void loginUser(String email, String password) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    try {
      emit(AuthBlocLoadingState());
      final result = await db.login(email, password);
      Timer(Duration(seconds: 2), () async {
        if (result != null) {
          await sharedPreferences.setBool(Preference.STATUS_LOGIN, true);
          await sharedPreferences.setString(
              Preference.USER_INFO, result.toString());
          emit(AuthBlocLoggedInState());
        } else {
          emit(
              AuthBlocErrorState('Login gagal , periksa kembali inputan anda'));
        }
      });
    } catch (e) {
      debugPrint(e.toString());
      emit(AuthBlocErrorState('error'));
    }
  }

  void registerUser(User user) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    try {
      emit(AuthBlocLoadingState());
      await db.register(user);
      Timer(Duration(seconds: 2), () async {
        String data = user.toJson().toString();
        await sharedPreferences.setBool(Preference.STATUS_LOGIN, true);
        await sharedPreferences.setString(Preference.USER_INFO, data);
        emit(AuthBlocLoggedInState());
      });
    } catch (e) {
      debugPrint(e.toString());
      emit(AuthBlocErrorState('error'));
    }
  }

  Future<void> logoutUser() async {
    try {
      emit(AuthBlocLoadingState());
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      Timer(Duration(seconds: 2), () async {
        await sharedPreferences.setBool(Preference.STATUS_LOGIN, false);
        await sharedPreferences.setString(Preference.USER_INFO, null);
        _isUserLogged = false;
        emit(AuthBlocLoginState());
      });
    } catch (e) {
      debugPrint(e.toString());
      emit(AuthBlocErrorState('error'));
    }
  }
}
