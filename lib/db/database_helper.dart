import 'package:majootestcase/models/user.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper {
  static DatabaseHelper _databaseHelper;
  DatabaseHelper._instance() {
    _databaseHelper = this;
  }

  factory DatabaseHelper() => _databaseHelper ?? DatabaseHelper._instance();

  static Database _database;

  Future<Database> get database async {
    if (_database == null) {
      _database = await _initDb();
    }
    return _database;
  }

  static const String _tblUsers = 'users';

  Future<Database> _initDb() async {
    final path = await getDatabasesPath();
    final databasePath = '$path/testcase_majoo.db';

    var db = await openDatabase(databasePath, version: 1, onCreate: _onCreate);
    return db;
  }

  void _onCreate(Database db, int version) async {
    await db.execute('''
      CREATE TABLE  $_tblUsers (
        email TEXT,
        username TEXT,
        password TEXT
      );
    ''');
  }

  Future<int> register(User user) async {
    final db = await database;
    return await db.insert(
      _tblUsers,
      user.toJson(),
    );
  }

  Future<Map<String, dynamic>> login(String email, String password) async {
    final db = await database;
    final results = await db.query(
      _tblUsers,
      where: 'email = ? and password = ?',
      whereArgs: [email, password],
    );

    if (results.isNotEmpty) {
      print(results.first);
      return results.first;
    } else {
      print('NULL');
      return null;
    }
  }
}
