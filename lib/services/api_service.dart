import 'package:dio/dio.dart';
import 'package:majootestcase/models/movie_model.dart';

import 'package:majootestcase/services/dio_config_service.dart' as dioConfig;
import 'package:majootestcase/utils/constant.dart';

class ApiServices {
  Future<MovieModel> getMovieList() async {
    var dio = await dioConfig.dio();

    Response response = await dio.get('/trending/all/day?api_key=${Api.KEY}');

    if (response.statusCode == 200) {
      return MovieModel.fromJson(response.data);
    } else {
      throw DioError();
    }
  }
}
