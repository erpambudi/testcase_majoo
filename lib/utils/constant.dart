class Preference {
  static const USER_INFO = "user_info";
  static const STATUS_LOGIN = "is_logged_in";
}

class Api {
  static const BASE_URL = "https://api.themoviedb.org/3";
  static const KEY = "0bc9e6490f0a9aa230bd01e268411e10";
}

class ScreenUtilConstants {
  static const width = 320.0;
  static const height = 640.0;
}
