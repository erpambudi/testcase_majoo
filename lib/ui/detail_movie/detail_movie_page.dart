import 'package:flutter/material.dart';
import 'package:majootestcase/models/movie_model.dart';
import 'package:majootestcase/utils/datetime_helper.dart';

class DetailMoviePage extends StatelessWidget {
  final Results movie;

  DetailMoviePage({Key key, this.movie}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          _header(context),
          _description(),
        ],
      ),
    );
  }

  Widget _header(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height / 2,
      width: double.infinity,
      child: Stack(
        children: [
          Container(
            height: double.infinity,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.black26,
            ),
            child: Center(
              child: Icon(
                Icons.image_search,
                size: 100,
                color: Colors.black54,
              ),
            ),
          ),
          Hero(
            tag: movie.id,
            child: Image.network(
              "https://image.tmdb.org/t/p/w500/" + movie.posterPath,
              fit: BoxFit.cover,
              width: double.infinity,
              errorBuilder: (BuildContext context, Object exception,
                  StackTrace stackTrace) {
                return Container(
                  child: Center(
                    child: Text(
                      'Image Not Found',
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.black54, fontSize: 20),
                    ),
                  ),
                );
              },
            ),
          ),
          Padding(
            padding: EdgeInsets.all(16),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: CircleAvatar(
                    backgroundColor: Colors.white.withOpacity(0.3),
                    child: Icon(
                      Icons.arrow_back,
                      color: Colors.black54,
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child: Container(
                        padding: EdgeInsets.all(12),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(16),
                          color: Colors.black.withOpacity(0.4),
                        ),
                        child: Text(
                          movie.originalTitle != null
                              ? movie.originalTitle
                              : movie.originalName != null
                                  ? movie.originalName
                                  : "-",
                          style: TextStyle(
                            fontSize: 24,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    Container(
                      width: 36,
                      height: 36,
                      decoration: BoxDecoration(
                        color: Colors.white.withOpacity(0.8),
                        borderRadius: BorderRadius.circular(6),
                      ),
                      child: Center(
                        child: Text(
                          movie.originalLanguage ?? '-',
                          style: TextStyle(
                            color: Colors.black54,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _description() {
    return Container(
      margin: EdgeInsets.fromLTRB(16, 12, 16, 12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              SizedBox(
                width: 20,
                child: Icon(
                  Icons.star,
                  color: Colors.orange[600],
                  size: 22,
                ),
              ),
              SizedBox(
                width: 8,
              ),
              SizedBox(
                width: 90,
                child: Text(
                  'Rating',
                  style: TextStyle(
                    color: Colors.black87,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
              SizedBox(
                width: 6,
                child: Text(
                  ':',
                  style: TextStyle(
                    color: Colors.black54,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
              Text(
                movie.voteAverage != null ? movie.voteAverage.toString() : "-",
                style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 4,
          ),
          Row(
            children: [
              SizedBox(
                width: 20,
                child: Icon(
                  Icons.calendar_today,
                  size: 18,
                  color: Colors.purple[300],
                ),
              ),
              SizedBox(
                width: 8,
              ),
              SizedBox(
                width: 90,
                child: Text(
                  'Release Date',
                  style: TextStyle(
                    color: Colors.black87,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
              SizedBox(
                width: 6,
                child: Text(
                  ':',
                  style: TextStyle(
                    color: Colors.black54,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
              Text(
                movie.releaseDate != null
                    ? DateTimeHelper.convertDateTime(movie.releaseDate)
                    : "-",
                style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 4,
          ),
          Row(
            children: [
              SizedBox(
                width: 20,
                child: Icon(
                  Icons.group,
                  color: Colors.blue[300],
                  size: 20,
                ),
              ),
              SizedBox(
                width: 8,
              ),
              SizedBox(
                width: 90,
                child: Text(
                  'Popularity',
                  style: TextStyle(
                    color: Colors.black87,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
              SizedBox(
                width: 6,
                child: Text(
                  ':',
                  style: TextStyle(
                    color: Colors.black54,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
              Text(
                movie.popularity != null ? movie.popularity.toString() : "-",
                style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            children: [
              SizedBox(
                child: Icon(
                  Icons.menu_book,
                  color: Colors.teal[300],
                  size: 20,
                ),
              ),
              SizedBox(
                width: 8,
              ),
              Text(
                'Overview :',
                style: TextStyle(
                  color: Colors.black87,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 4,
          ),
          Text(
            movie.overview,
            style: TextStyle(
              fontSize: 16,
              color: Colors.black45,
              fontWeight: FontWeight.w500,
            ),
          ),
        ],
      ),
    );
  }
}
