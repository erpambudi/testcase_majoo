import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/ui/auth/login_page.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_screen.dart';

class SplashScreenPage extends StatefulWidget {
  const SplashScreenPage({Key key}) : super(key: key);

  @override
  _SplashScreenPageState createState() => _SplashScreenPageState();
}

class _SplashScreenPageState extends State<SplashScreenPage> {
  startSplashScreen() async {
    final authBloc = BlocProvider.of<AuthBlocCubit>(context);
    return Timer(
      const Duration(milliseconds: 1500),
      () {
        if (authBloc.isUserLogged) {
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(
                builder: (_) => BlocProvider(
                  create: (context) => HomeBlocCubit()..fetchingData(),
                  child: HomeBlocScreen(),
                ),
              ),
              (route) => false);
        } else {
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(
                builder: (_) => BlocProvider(
                  create: (context) => AuthBlocCubit(),
                  child: LoginPage(),
                ),
              ),
              (route) => false);
        }
      },
    );
  }

  @override
  void initState() {
    startSplashScreen();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              Icons.movie,
              size: 100,
              color: Colors.blue,
            ),
            Text(
              'My Movies',
              style: TextStyle(
                fontSize: 24,
                color: Colors.black54,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
