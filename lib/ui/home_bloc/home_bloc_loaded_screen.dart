import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/models/movie_model.dart';
import 'package:majootestcase/ui/auth/login_page.dart';
import 'package:majootestcase/ui/detail_movie/detail_movie_page.dart';

class HomeBlocLoadedScreen extends StatelessWidget {
  final List<Results> data;

  HomeBlocLoadedScreen({Key key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          ListView.builder(
            physics: BouncingScrollPhysics(),
            itemCount: data.length,
            scrollDirection: Axis.vertical,
            itemBuilder: (context, i) {
              return Padding(
                padding: EdgeInsets.only(
                  top: (data[i] == data.first) ? 22 : 12,
                  bottom: (data[i] == data.last) ? 22 : 0,
                ),
                child: GestureDetector(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return DetailMoviePage(movie: data[i]);
                      }));
                    },
                    child: movieItem(data[i])),
              );
            },
          ),
          Positioned(
            right: 10,
            bottom: 20,
            child: LogOutIcon(),
          ),
        ],
      ),
    );
  }

  Widget movieItem(Results movie) {
    return Container(
      height: 200,
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: 20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(24),
      ),
      child: Stack(
        children: [
          Container(
            height: double.infinity,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.black26,
              borderRadius: BorderRadius.circular(24),
            ),
            child: Center(
              child: Icon(
                Icons.image_search,
                size: 50,
                color: Colors.black54,
              ),
            ),
          ),
          Hero(
            tag: movie.id,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(24),
              child: Image.network(
                "https://image.tmdb.org/t/p/w500/" + movie.backdropPath,
                fit: BoxFit.cover,
                height: double.infinity,
                width: double.infinity,
                loadingBuilder: (BuildContext context, Widget child,
                    ImageChunkEvent loadingProgress) {
                  if (loadingProgress == null) {
                    return child;
                  }
                  return Container(
                    height: double.infinity,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color: Colors.black26,
                    ),
                    child: Center(
                      child: Icon(
                        Icons.image_search,
                        size: 50,
                        color: Colors.black54,
                      ),
                    ),
                  );
                },
                errorBuilder: (BuildContext context, Object exception,
                    StackTrace stackTrace) {
                  return Container(
                    child: Center(
                      child: Text(
                        'Image Not Found',
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.black54, fontSize: 20),
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
          Positioned(
            top: 12,
            left: 12,
            child: Container(
              width: 36,
              height: 36,
              decoration: BoxDecoration(
                color: Colors.white.withOpacity(0.8),
                borderRadius: BorderRadius.circular(6),
              ),
              child: Center(
                child: Text(
                  movie.originalLanguage ?? '-',
                  style: TextStyle(
                    color: Colors.black54,
                  ),
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              margin: EdgeInsets.fromLTRB(18, 8, 18, 18),
              child: Row(
                children: [
                  Expanded(
                    child: Text(
                      movie.originalTitle != null
                          ? movie.originalTitle
                          : movie.originalName != null
                              ? movie.originalName
                              : "-",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Container(
                    child: Text(
                      movie.voteAverage.toString(),
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class LogOutIcon extends StatefulWidget {
  const LogOutIcon({Key key}) : super(key: key);

  @override
  _LogOutIconState createState() => _LogOutIconState();
}

class _LogOutIconState extends State<LogOutIcon> {
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return BlocProvider<AuthBlocCubit>(
      create: (context) => AuthBlocCubit(),
      child: BlocListener<AuthBlocCubit, AuthBlocState>(
        listener: (context, state) {
          if (state is AuthBlocLoadingState) {
            setState(() {
              _isLoading = true;
            });
          } else if (state is AuthBlocErrorState) {
            setState(() {
              _isLoading = false;
            });
            Scaffold.of(context).showSnackBar(
              SnackBar(
                content: Text(state.error),
                backgroundColor: Colors.red,
              ),
            );
          } else if (state is AuthBlocLoginState) {
            Scaffold.of(context).showSnackBar(
              SnackBar(
                content: Text('Logout Berhasil'),
                backgroundColor: Colors.blue,
              ),
            );
            Timer(Duration(seconds: 1), () {
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(
                    builder: (_) => BlocProvider(
                      create: (context) => AuthBlocCubit(),
                      child: LoginPage(),
                    ),
                  ),
                  (route) => false);
            });
          }
        },
        child: Builder(
          builder: (context) {
            return GestureDetector(
              onTap: () {
                BlocProvider.of<AuthBlocCubit>(context).logoutUser();
              },
              child: SizedBox(
                height: 50,
                width: 50,
                child: CircleAvatar(
                  backgroundColor: Colors.lightBlue,
                  child: _isLoading
                      ? SizedBox(
                          height: 25,
                          width: 25,
                          child: CircularProgressIndicator(
                            backgroundColor: Colors.white,
                          ),
                        )
                      : Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.logout,
                              color: Colors.white,
                            ),
                            Text(
                              'Logout',
                              style: TextStyle(
                                fontSize: 7,
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
